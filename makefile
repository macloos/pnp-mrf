dictionary:
	make -f makefile.dictionary

simulation:
	make -f makefile.simulation

recon:
	make -f makefile.recon

inversion:
	make  -f makefile.inversion

mrf:
########################################
# Tools for a virtual mrf experiment
########################################
		make clean_mrf
		make -f make_scripts/makefile.mrf_simulation
		make -f make_scripts/makefile.mrf_dictionary
		make -f make_scripts/makefile.mrf_recon
		make -f make_scripts/makefile.inversion


pnp:
########################################
# Tools for a virtual pnp-mrf experiment
########################################
		make clean_pnp
		make -f make_scripts/makefile.pnp_simulation
		make -f make_scripts/makefile.pnp_dictionary
		make -f make_scripts/makefile.pnp_recon


clean_mrf:
########################################
# Tools for a virtual mrf experiment
########################################
	make clean -f make_scripts/makefile.mrf_dictionary
	make clean -f make_scripts/makefile.mrf_simulation
	make clean -f make_scripts/makefile.mrf_recon
	make clean -f make_scripts/makefile.inversion


clean_pnp:
########################################
# Tools for a virtual pnp-mrf experiment
########################################
	make clean -f make_scripts/makefile.pnp_dictionary
	make clean -f make_scripts/makefile.pnp_simulation
	make clean -f make_scripts/makefile.pnp_recon


########################################
# scrub data files from source
########################################
clean_data:
	rm -f *.float
	rm -f .DS_Store
	rm -f EPG_CODE/.DS_Store
	rm -f Adiabatic_Inversion/.DS_Store
	rm -f BRAIN_MODEL/.DS_Store
	rm -f SEQ_MRF/.DS_Store
	rm -f SEQ_PNP/.DS_Store
	rm -f SRC/.DS_Store


########################################
# retrun to source only.
########################################
clean:
	make clean_mrf
	make clean_pnp
	make clean_data
	rm -f *.app


########################################
# A quick run sequence for pnp-mrf
########################################
run_pnp:
	./Create_PNP_Dictionary.app
	./Simulate_PNP_Acquisition.app
	./Recon_PNP.app


########################################
# A quick run sequence for mrf
########################################
run_mrf:
	./Create_MRF_Dictionary.app
	./Simulate_MRF_Acquisition.app
	./Recon_MRF.app
