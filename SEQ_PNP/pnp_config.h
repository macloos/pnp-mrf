#ifndef __PNP_CONFIG_H__
#define __PNP_CONFIG_H__

#define SIZE_OF_FINGERPRINT   480
#define COMPRESSION_FACTOR    15
#define SIZE_OF_COMPRESSED_FINGERPRINT SIZE_OF_FINGERPRINT/COMPRESSION_FACTOR

#define NUMBER_OF_TX_CHANNELS 2
// #define SPAT_STEPS 1  //no slice profile
#define SPAT_STEPS 1  //slice profile for a time bandwithd factor 3

// Some numbers
#define DEG_2_RAD 0.0174533f
#define PI   3.14159f

#ifdef SAVE_AS_FLOAT
  typedef float SAVEFORMAT;
#else
  typedef complex<float> SAVEFORMAT;
#endif

#endif
