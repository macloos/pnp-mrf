#ifndef __LOOK_UP_TABLE_HEADER_HPP__
#define __LOOK_UP_TABLE_HEADER_HPP__

#include <iostream>
#include <fstream>
#include <vector>

namespace pnp_mrf{

typedef struct lookup_table_header{
  long  l_size_of_database_file;
  long  l_Dim;
  long  l_size_of_compressed_fingerprint;
  float f_TR;
  float f_TBW;

  //Array dimensions
  long l_PD;    //size of PD dimension (1)
  long l_T2m;   //size of T2 dimension
  long l_T1m;   //size of T1 dimension
  long l_B2m;   //size of B1 dimension for TX2
  long l_B1m;   //size of B1 dimeneion for TX1

  //min
  float f_min_PD;
  float f_min_T2;
  float f_min_T1;
  float f_min_B2;
  float f_min_B1;

  //max
  float f_max_PD;
  float f_max_T2;
  float f_max_T1;
  float f_max_B2;
  float f_max_B1;

  //size of labels
  int i_size_of_label_PD;
  int i_size_of_label_T2;
  int i_size_of_label_T1;
  int i_size_of_label_B1;
  int i_size_of_label_B2;

  // lables
  char c_label_PD[100];
  char c_label_T2[100];
  char c_label_T1[100];
  char c_label_B2[100];
  char c_label_B1[100];
}lookup_table_header_t;



//==============================================
// Load lookup table from file
//==============================================
template<typename T>
lookup_table_header load_lookup(const char* file_name, std::vector<T> &data){
  std::cout << "Loading: " << file_name << " \n\033[F\033[J";

  lookup_table_header header;
  data.clear();
  std::ifstream input( file_name, std::ios::binary );
  if(input.is_open()){
    input.read((char*)&header, sizeof(header));

    data.reserve(header.l_size_of_database_file * header.l_Dim);

    T buffer;
    while(input.read((char *)&buffer,sizeof(T))){
      data.push_back(buffer);
    }
  }else{
    std::cout << "Failed to load: " << file_name << std::endl;
    exit(0);
  }

  return header;
}

} //namespace lookup_table
#endif
