%% Navigate to the SEQ_MRF folder before running script

%% Show the relative RF amplitudes
fileID = fopen('SEQ_MRF/MRF_FA.double','r');
TX1 = fread(fileID,1000,'double');
fclose(fileID);

figure; 
plot(60*TX1);
title('Flip-angle train in the MRF sequence');
xlabel('Exposure Index');
ylabel('Flip-angle (Deg)');
legend('Channel 1');

%% Show the RF phases
fileID = fopen('SEQ_MRF/MRF_PH.double','r');
Ph1 = fread(fileID,1000,'double');
fclose(fileID);

figure; 
plot(Ph1);
title('Transmit phase in the MRF sequence');
xlabel('Exposure Index');
ylabel('Phase');
legend('Channel 1');

%% Show the TR
fileID = fopen('SEQ_MRF/MRF_TR.double','r');
TR = fread(fileID,1000,'double');
fclose(fileID);

figure; 
plot(TR);
title('Inter echo time in the MRF sequence');
xlabel('Exposure Index');
ylabel('Time in ms');