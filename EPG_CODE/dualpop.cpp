#include "dualpop.h"


#define GETSIGN(a) (((a)<0.0f)?(-1.0f):(1.0f))


dualpop::dualpop(){
}

dualpop::~dualpop(){
}

///---------------------------------------------
/// Initialize 2013 MRF sequence simulation
///---------------------------------------------
void dualpop::mrf_init(double **dRF_sequence,
                       double **dPh_sequence,
                       double  *dTR_sequence,
                       float f_TX1_flip,
                       float fT1,
                       float fT2,
                       float f_PD,
                       float f_In){


    float f_pro_flipangle_inc = 1.0/SPAT_STEPS;

    this->f_PD   = f_PD;
    this->f_In   = f_In;

    for(int i=0; i<SPAT_STEPS; i++){
        f_pro_scale[i] = 1.0;

        pro_pool[i].f_flipangle[0] = f_pro_flipangle_inc*(i+1)*f_TX1_flip;
        pro_pool[i].f_flipangle[1] = 0.0f;
        pro_pool[i].init(dRF_sequence,dPh_sequence,dTR_sequence,fT1,fT2);
    }

}

///---------------------------------------------
/// Initialize 2016 PnP-MRF sequence simulation
///---------------------------------------------
void dualpop::pnp_init(double **dRF_sequence,
                       double **dPh_sequence,
                       double  *dTR_sequence,
                       float f_TX1_flip,
                       float f_TX2_flip,
                       float fT1,
                       float fT2,
                       float f_PD){

    float f_pro_flipangle_inc = 1.0/SPAT_STEPS;

    this->f_PD   = f_PD;
    this->f_In   = 1;

    if( SPAT_STEPS == 8){ //corresponds to a time bandwith factor of 3
        f_pro_scale[0] = 0.068966f;
        f_pro_scale[1] = 0.137931f;
        f_pro_scale[2] = 0.137931f;
        f_pro_scale[3] = 0.068966f;
        f_pro_scale[4] = 0.137931f;
        f_pro_scale[5] = 0.068966f;
        f_pro_scale[6] = 0.206897f;
        f_pro_scale[7] = 0.172414f;
    }else{
      f_pro_scale[0] = 1.0;
    }

    for(int i=0; i<SPAT_STEPS; i++){

        pro_pool[i].f_flipangle[0] = f_pro_flipangle_inc*(i+1)*f_TX1_flip;
        pro_pool[i].f_flipangle[1] = f_pro_flipangle_inc*(i+1)*f_TX2_flip;
        pro_pool[i].init(dRF_sequence,dPh_sequence,dTR_sequence,fT1,fT2);
    }

}


///---------------------------------------------
/// Run 2013 MRF sequence simulation
///---------------------------------------------
void dualpop::mrf_sim(SAVEFORMAT *f_fingerprint){

    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].reset(f_PD);
        pro_pool[i].inversion(f_In); //inversion
    }

    for(int i=0; i<SIZE_OF_FINGERPRINT;i++){
      update_TR();
      relax();
      rf();

      f_fingerprint[i] = get_signal();
    }
}


///---------------------------------------------
/// Run 2016  Nature Communications PnP-MRF sequence simulation
///---------------------------------------------
void dualpop::pnp_sim(SAVEFORMAT *f_fingerprint){

    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].reset(f_PD);
    }

    for(int i=0; i<SIZE_OF_FINGERPRINT;i++){

        if(i% 120==0){
            delay(240);
        }

        if(i < 120){
            spoil();
        }else if(i < 240){
            relax();
            shift();
        }else if(i < 360){
            spoil();
        }else{
            relax();
            shift();
        }

        rf();

        f_fingerprint[i] = get_abs_signal();
    }
}


SAVEFORMAT dualpop::get_signal(){
    complex<float> cf_tmp = 0.f;

    for(int i=0; i<SPAT_STEPS; i++){
        cf_tmp += f_pro_scale[i]*pro_pool[i].get_signal();
    }
    #ifdef SAVE_AS_FLOAT
      return -1.0f*abs(cf_tmp)*GETSIGN(arg(cf_tmp)); // signed signal magnitude [-1, 1]
    #else
      return cf_tmp; // complex signal
    #endif
}

SAVEFORMAT dualpop::get_abs_signal(){
    complex<float> cf_tmp = 0.f;

    for(int i=0; i<SPAT_STEPS; i++){
        cf_tmp += abs(f_pro_scale[i]*pro_pool[i].get_signal());
    }
    #ifdef SAVE_AS_FLOAT
      return abs(cf_tmp);
    #else
      return cf_tmp; // complex signal
    #endif
}

void dualpop::spoil(){
    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].spoil();
    }
}

void dualpop::update_TR(){
    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].update_TR();
    }
}

void dualpop::relax(){
    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].relax();
    }
}


void dualpop::shift(){
    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].shift();
    }
}

void dualpop::delay(int iLength){
      for(int i=0; i<SPAT_STEPS; i++){
        for(int j=0; j<iLength; j++){
          pro_pool[i].spoil();
          }
      }
}

void dualpop::rf(){
    for(int i=0; i<SPAT_STEPS; i++){
        pro_pool[i].rf();
    }
}
