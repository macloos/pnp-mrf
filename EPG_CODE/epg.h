#ifndef __EPG_H__

#define __EPG_H__
#include <cmath>
#include <complex>
#include <iostream>

#ifdef MRFCONFIG
  #include "../SEQ_MRF/mrf_config.h"
#else
  #include "../SEQ_PNP/pnp_config.h"
#endif

using namespace std;

class epg{

public:
   epg();
  ~epg();

  void init(double **dRF_seq, double **dPh_seq, double *dTR_seq, float fT1, float fT2);

  void reset(float f_poolsize);
  void inversion(float f_Inversion_Factor);
  void update_TR();
  void relax();
  void shift();
  void spoil();
  void rf();

  complex<float> get_signal();
  float f_flipangle[NUMBER_OF_TX_CHANNELS];

private:
  void clear();
  void update_RF_operator();

  float m_fT1_factor;
  float m_fT2_factor;
  float m_fT1;
  float m_fT2;

  int i_current_step;
  int i_max_number_of_pathways;

  double** m_dRF_sequence; //RF amplitude for each TX Channel
  double** m_dPh_sequence; //RF phase for each TX Channel
  double*  m_dTR_sequence; //Delay between RF pulses

  float f_poolsize;

  complex<float> (*cf_epg_matrix_pro)[3]; //Free Protons
  complex<float> cf_rot_matrix[3][3];
  complex<float> t_epg[3];  //temporary variable for fast RF operator application

  float t_sin1;             //temporary variable for fast RF operator calculation
  float t_cos2;             //temporary variable for fast RF operator calculation
  float t_sin2;             //temporary variable for fast RF operator calculation
  float t_fa_abs;           //temporary variable for fast RF operator calculation
  float t_fa_pha;           //temporary variable for fast RF operator calculation
  complex<float> t_fa;      //temporary variable for fast RF operator calculation

};

#endif
