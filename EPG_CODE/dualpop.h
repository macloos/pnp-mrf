#ifndef __DUALPOP_H__

#define __DUALPOP_H__
#include <cmath>
#include <complex>
#include <iostream>

#ifdef MRFCONFIG
  #include "../SEQ_MRF/mrf_config.h"
#else
  #include "../SEQ_PNP/pnp_config.h"
#endif

#include "epg.h"


using namespace std;

class dualpop{

public:
  ~dualpop();
   dualpop();

  void mrf_init(double **dRF_sequence,
              double **dPh_sequence,
              double  *dTR_sequence,
              float f_TX1_flip,
              float fT1,
              float fT2,
              float f_PD =  1.0f,
              float f_In = -1.0f);

  void pnp_init(double **dRF_sequence,
              double **dPh_sequence,
              double  *dTR_sequence,
              float f_TX1_flip,
              float f_TX2_flip,
              float fT1,
              float fT2,
              float f_PD = 1.0f);

  void mrf_sim(SAVEFORMAT *f_fingerprint); //MRF sequnece by Ma et al., Nature 2013
  void pnp_sim(SAVEFORMAT *f_fingerprint); //PnP-MRF sequence by Cloos et al., Nature Communications 2015

private:
    void spoil();                  // Spoil all trensverse magnetization
    void update_TR();              // Update the delay time to next TR
    void relax();                  // Relax spins for the duration of 1 TR
    void shift();                  // Shift the coherence pathways
    void delay(int iLength);       // Spoil and Relax T1 for 240 x the TR
    void rf();                     // RF operator

    SAVEFORMAT get_signal(); //get the transverse magnetization
    SAVEFORMAT get_abs_signal(); //get the transverse magnetization

    float f_PD;
    float f_In;

    // The following can be used to incorporate the slice profile.
    // To this end, the slice profile is segmented in "SPAT_STEPS" segments.
    // Each segment simlates the different B1 observed by each position in the slice
    epg pro_pool[SPAT_STEPS];       //Array of EPG simulations
    float f_pro_scale[SPAT_STEPS];  //Relative weights of each part of the slice profile
};

#endif
