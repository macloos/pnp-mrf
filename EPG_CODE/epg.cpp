#include "epg.h"

epg::epg(){
  cf_epg_matrix_pro = 0;
  cf_epg_matrix_pro = new complex<float>[SIZE_OF_FINGERPRINT+3][3];
}

epg::~epg(){
  clear();
}

///---------------------------------------------
/// Initialize memory
///---------------------------------------------
void epg::init(double **dRF_seq, double **dPh_seq, double *dTR_seq, float fT1, float fT2){
  m_dRF_sequence = dRF_seq;
  m_dPh_sequence = dPh_seq;
  m_dTR_sequence = dTR_seq;
  m_fT1 = fT1;
  m_fT2 = fT2;

  m_fT1_factor=exp(-1.0*m_dTR_sequence[0]/m_fT1);
  m_fT2_factor=exp(-1.0*m_dTR_sequence[0]/m_fT2);
}



///---------------------------------------------
///---------------------------------------------
/// Private Functions
///---------------------------------------------
///---------------------------------------------
complex<float> epg::get_signal(){
    return polar(1.0f, -1.f * t_fa_pha)*cf_epg_matrix_pro[0][0];  //shift ADC with RF phase
}

///---------------------------------------------
/// Release the memory
///---------------------------------------------
void epg::clear(){
    if(cf_epg_matrix_pro!=0){
      delete [] cf_epg_matrix_pro;
    }
}

///---------------------------------------------
/// Apply inversion pulse
///---------------------------------------------
void epg::inversion(float f_Inversion_Factor){
  for(int i=0; i<SIZE_OF_FINGERPRINT+3; i++){
    cf_epg_matrix_pro[i][2]= f_Inversion_Factor* cf_epg_matrix_pro[i][2];
  }
}

///---------------------------------------------
/// Reset the simulation
///---------------------------------------------
void epg::reset(float f_poolsize){
    this->f_poolsize=f_poolsize;

    for(int i=0; i<SIZE_OF_FINGERPRINT+3; i++){
        cf_epg_matrix_pro[i][0]=0.0f;
        cf_epg_matrix_pro[i][1]=0.0f;
        cf_epg_matrix_pro[i][2]=0.0f;
    }

    cf_epg_matrix_pro[0][2]=f_poolsize;

    i_current_step=0;
    i_max_number_of_pathways=2;
}

///---------------------------------------------
/// Update the TR
///---------------------------------------------
void epg::update_TR(){
        m_fT1_factor=exp(-m_dTR_sequence[i_current_step]/m_fT1);
        m_fT2_factor=exp(-m_dTR_sequence[i_current_step]/m_fT2);
}

///---------------------------------------------
/// apply relaxation operator
///---------------------------------------------
void epg::relax(){
    for(int i=1; i<i_max_number_of_pathways+1; i++){
        cf_epg_matrix_pro[i][0]*=m_fT2_factor;
        cf_epg_matrix_pro[i][1]*=m_fT2_factor;
        cf_epg_matrix_pro[i][2]*=m_fT1_factor;
    }

    cf_epg_matrix_pro[0][0]*=m_fT2_factor;
    cf_epg_matrix_pro[0][1]*=m_fT2_factor;
    cf_epg_matrix_pro[0][2]*=m_fT1_factor;

    cf_epg_matrix_pro[0][2]+=f_poolsize*(1-m_fT1_factor);

    // cout<<"-RELAX-"<<endl;
    // for(int i=0; i<i_current_step+1; i++){
    //     cout<<cf_epg_matrix_pro[i][0]<<" "<<cf_epg_matrix_pro[i][1]<<" "<<cf_epg_matrix_pro[i][2]<<endl;
    // }
}

///---------------------------------------------
/// apply spoiler operator
///---------------------------------------------
void epg::spoil(){
    cf_epg_matrix_pro[0][0]=0.f;
    cf_epg_matrix_pro[0][1]=0.f;
    cf_epg_matrix_pro[0][2]*=m_fT1_factor;
    cf_epg_matrix_pro[0][2] =abs(cf_epg_matrix_pro[0][2]);
    for(int i=1; i<i_max_number_of_pathways+1; i++){
        cf_epg_matrix_pro[i][0]=0.f;
        cf_epg_matrix_pro[i][1]=0.f;
        cf_epg_matrix_pro[i][2]=0.f;
    }

    cf_epg_matrix_pro[0][2]+=f_poolsize*(1-m_fT1_factor);

    i_max_number_of_pathways=2;

    // cout<<"-RELAX-"<<endl;
    // for(int i=0; i<i_current_step+1; i++){
    //     cout<<cf_epg_matrix_pro[i][0]<<" "<<cf_epg_matrix_pro[i][1]<<" "<<cf_epg_matrix_pro[i][2]<<endl;
    // }
}


///---------------------------------------------
/// apply shift operator for FISP sequence
///---------------------------------------------
void epg::shift(){
  i_max_number_of_pathways++;
  // cout<<"-PRE-SHIFT-"<<endl;
  // for(int i=0; i<i_current_step+1; i++){
  //   cout<<cf_epg_matrix_pro[i][0]<<" "<<cf_epg_matrix_pro[i][1]<<" "<<cf_epg_matrix_pro[i][2]<<endl;
  // }

  for (int i = 0; i < i_current_step; i++){
    cf_epg_matrix_pro[i_current_step-i][0] = cf_epg_matrix_pro[i_current_step - i - 1][0];
    cf_epg_matrix_pro[i               ][1] = cf_epg_matrix_pro[i + 1                 ][1];
  }

  cf_epg_matrix_pro[0][0]=conj(cf_epg_matrix_pro[0][1]);

  // cout<<"-POS-SHIFT-"<<endl;
  // for(int i=0; i<i_current_step+1; i++){
  //   cout<<cf_epg_matrix_pro[i][0]<<" "<<cf_epg_matrix_pro[i][1]<<" "<<cf_epg_matrix_pro[i][2]<<endl;
  // }
}

///---------------------------------------------
/// prepare RF operator
///---------------------------------------------
void epg::update_RF_operator(){
    t_fa = f_flipangle[0]*polar((float) m_dRF_sequence[0][i_current_step],(float) (m_dPh_sequence[0][i_current_step]));
    t_fa+= f_flipangle[1]*polar((float) m_dRF_sequence[1][i_current_step],(float) (m_dPh_sequence[1][i_current_step]));

    t_fa_abs = abs(t_fa) * DEG_2_RAD;
    t_fa_pha = arg(t_fa);

    t_sin1 = sin(t_fa_abs);
    t_cos2 = pow(cos(t_fa_abs/2.0f),2);
    t_sin2 = pow(sin(t_fa_abs/2.0f),2);

    // if(i_current_step==0){
    //   cout<<"t_cos2"<<t_cos2<<endl;
    //   cout<<"t_pha*t_pha*t_sin2"<<t_pha*t_pha*t_sin2<<endl;
    // }


    // row 1
    cf_rot_matrix[0][0] =                                       t_cos2;
    cf_rot_matrix[0][1] =          polar( t_sin2,       2.f * t_fa_pha);
    cf_rot_matrix[0][2] =   -1.f * polar( t_sin1, t_fa_pha + 0.5f * PI);

    // row 2
    cf_rot_matrix[1][0] =   conj(cf_rot_matrix[0][1]);
    cf_rot_matrix[1][1] =                      t_cos2;
    cf_rot_matrix[1][2] =   conj(cf_rot_matrix[0][2]);

    //row 3
    cf_rot_matrix[2][0] =   polar(0.5f * t_sin1, -0.5f * PI - t_fa_pha);
    cf_rot_matrix[2][1] =   polar(0.5f * t_sin1,  t_fa_pha + 0.5f * PI);
    cf_rot_matrix[2][2] =                                 cos(t_fa_abs);

  // cout<<"-ROT-"<<endl;
  // for(int i=0; i<3; i++){
  //     cout<<cf_rot_matrix[i][0]<<" "<<cf_rot_matrix[i][1]<<" "<<cf_rot_matrix[i][2]<<endl;
  // }
}

///---------------------------------------------
/// apply RF operator
///---------------------------------------------
void epg::rf(){

    update_RF_operator();

    for(int i=0; i<i_max_number_of_pathways+1; i++){
        t_epg[0] = cf_rot_matrix[0][0]*cf_epg_matrix_pro[i][0];
        t_epg[1] = cf_rot_matrix[1][0]*cf_epg_matrix_pro[i][0];
        t_epg[2] = cf_rot_matrix[2][0]*cf_epg_matrix_pro[i][0];

        t_epg[0]+= cf_rot_matrix[0][1]*cf_epg_matrix_pro[i][1];
        t_epg[1]+= cf_rot_matrix[1][1]*cf_epg_matrix_pro[i][1];
        t_epg[2]+= cf_rot_matrix[2][1]*cf_epg_matrix_pro[i][1];

        cf_epg_matrix_pro[i][0] = t_epg[0] + cf_rot_matrix[0][2]*cf_epg_matrix_pro[i][2];
        cf_epg_matrix_pro[i][1] = t_epg[1] + cf_rot_matrix[1][2]*cf_epg_matrix_pro[i][2];
        cf_epg_matrix_pro[i][2] = t_epg[2] + cf_rot_matrix[2][2]*cf_epg_matrix_pro[i][2];
    }

    i_current_step++;
    // cout<<"-RF-"<<endl;
    // for(int i=0; i<i_current_step+1; i++){
    //   cout<<cf_epg_matrix_pro[i][0]<<" "<<cf_epg_matrix_pro[i][1]<<" "<<cf_epg_matrix_pro[i][2]<<endl;
    // }
}
