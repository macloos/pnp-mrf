#include "pulse.h"

#define DEG_2_RAD 0.0174533f

pulse::pulse(){
  cf_pulse_matrix_pro = 0;
  t_im = complex<float>(0.0f,1.0f);
}

pulse::~pulse(){
  clear();
}

///---------------------------------------------
/// Initialize memory
///---------------------------------------------
void pulse::init(double *d_RFamp, double *d_RFpha, int i_size_of_pulse){
  clear();

  this->d_RFamp       = d_RFamp;
  this->d_RFpha       = d_RFpha;
  this->i_size_of_pulse = i_size_of_pulse;

  cf_pulse_matrix_pro = new complex<float>[3][3];

}


float pulse::sim(double d_b1){
  reset();
  this->d_b1=d_b1;

  for(int i=0; i<i_size_of_pulse; i++){
  // for(int i=0; i<100; i++){
    update_RF_operator();
    rf();
  }
   return get_mz();
}


///---------------------------------------------
///---------------------------------------------
/// Private Functions
///---------------------------------------------
///---------------------------------------------

float pulse::get_mz(){
    return abs(cf_pulse_matrix_pro[0][2]) * real(cf_pulse_matrix_pro[0][2])/abs(real(cf_pulse_matrix_pro[0][2]));
}

///---------------------------------------------
/// Release the memory
///---------------------------------------------
void pulse::clear(){
    if(cf_pulse_matrix_pro!=0){
      delete [] cf_pulse_matrix_pro;
    }
}


///---------------------------------------------
/// Reset the simulation
///---------------------------------------------
void pulse::reset(){

    for(int i=0; i<3; i++){
        cf_pulse_matrix_pro[i][0]=0.0f;
        cf_pulse_matrix_pro[i][1]=0.0f;
        cf_pulse_matrix_pro[i][2]=0.0f;
    }

    cf_pulse_matrix_pro[0][2]=1.0f;

    i_current_step=0;
    i_max_number_of_pathways=2;
}

///---------------------------------------------
/// prepare RF operator
///---------------------------------------------
void pulse::update_RF_operator(){
    t_pha = polar(1.0f, (float) d_RFpha[i_current_step]);
    // cout<<d_b1<<endl;//
    // cout<<d_RFamp[i_current_step]<<endl;
    t_fa  = d_b1*d_RFamp[i_current_step];
    t_fa *= DEG_2_RAD;

    t_sin1 = sin(t_fa);
    t_cos2 = pow(cos(t_fa/2.0f),2);
    t_sin2 = pow(sin(t_fa/2.0f),2);

    // row 1
    cf_rot_matrix[0][0] =                      t_cos2;
    cf_rot_matrix[0][1] =          t_pha*t_pha*t_sin2;
    cf_rot_matrix[0][2] =     conj(t_im)*t_pha*t_sin1;

    // row 2
    cf_rot_matrix[1][0] =   conj(cf_rot_matrix[0][1]);
    cf_rot_matrix[1][1] =                      t_cos2;
    cf_rot_matrix[1][2] =   conj(cf_rot_matrix[0][2]);

    //row 3
    cf_rot_matrix[2][0] =conj(t_pha*t_im)/2.0f*t_sin1;
    cf_rot_matrix[2][1] =      t_im*t_pha/2.0f*t_sin1;
    cf_rot_matrix[2][2] =                   cos(t_fa);

  // cout<<"-ROT-"<<endl;
  // for(int i=0; i<3; i++){
  //     cout<<cf_rot_matrix[i][0]<<" "<<cf_rot_matrix[i][1]<<" "<<cf_rot_matrix[i][2]<<endl;
  // }
}

///---------------------------------------------
/// apply RF operator
///---------------------------------------------
void pulse::rf(){

    for(int i=0; i<i_max_number_of_pathways+1; i++){
        t_pulse[0]=0.0f;
        t_pulse[1]=0.0f;
        t_pulse[2]=0.0f;
        for(int j=0; j<3; j++){
            t_pulse[0]+= cf_rot_matrix[0][j]*cf_pulse_matrix_pro[i][j];
            t_pulse[1]+= cf_rot_matrix[1][j]*cf_pulse_matrix_pro[i][j];
            t_pulse[2]+= cf_rot_matrix[2][j]*cf_pulse_matrix_pro[i][j];
        }
        cf_pulse_matrix_pro[i][0]=t_pulse[0];
        cf_pulse_matrix_pro[i][1]=t_pulse[1];
        cf_pulse_matrix_pro[i][2]=t_pulse[2];
    }

    i_current_step++;
    // cout<<"-RF-"<<endl;
    // for(int i=0; i<i_current_step+1; i++){
    //   cout<<cf_pulse_matrix_pro[i][0]<<" "<<cf_pulse_matrix_pro[i][1]<<" "<<cf_pulse_matrix_pro[i][2]<<endl;
    // }
}
