#ifndef __PULSE_H__

#define __PULSE_H__
#include <math.h>
#include <complex.h>
#include <iostream>

using namespace std;

class pulse{

public:
   pulse();
  ~pulse();

  void  init(double *d_RFamp, double *d_RFpha, int i_size_of_pulse);
  float sim (double d_b1);



private:
  void clear();
  void reset();
  void update_RF_operator();
  void rf();
  float get_mz();

  int i_size_of_pulse;
  int i_current_step;
  int i_max_number_of_pathways;

  double* d_RFamp;
  double* d_RFpha;
  double d_b1;

  complex<float> (*cf_pulse_matrix_pro)[3]; //Free Protons
  complex<float> cf_rot_matrix[3][3];
  complex<float> t_pulse[3];  //temporary variable for fast RF operator application

  float t_fa;               //temporary variable for fast RF operator calculation
  float t_sin1;             //temporary variable for fast RF operator calculation
  float t_cos2;             //temporary variable for fast RF operator calculation
  float t_sin2;             //temporary variable for fast RF operator calculation
  complex<float> t_pha;     //temporary variable for fast RF operator calculation
  complex<float> t_im;      //temporary variable for fast RF operator calculation

};

#endif
