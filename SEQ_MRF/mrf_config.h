#ifndef __MRF_CONFIG_H__
#define __MRF_CONFIG_H__

#define SIZE_OF_FINGERPRINT   1000
#define NUMBER_OF_TX_CHANNELS 1
#define SPAT_STEPS 1  //Ma et al. Nature 2013 (no slice profile)

// Some numbers
#define DEG_2_RAD 0.0174533f
#define PI  3.14159f

#ifdef SAVE_AS_FLOAT
  typedef float SAVEFORMAT;
#else
  typedef SAVEFORMAT complex<float>
#endif

#endif
