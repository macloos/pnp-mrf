//////////////////////////////////////////////////////////
// Project Name :   Adibatic Inversion Fidelity Simulator
// Discription  :   This program simulates the flip achived
//                  using an adibatic pulse
//
//                  This simulation was used for examlple 1 and 2 in:
//                  Cloos et al., Nature Communications 2016
//
// Author       :   Martijn Cloos
// Email: martijn.cloos@nyumc.org
//////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <complex>
#include <cmath>


#include "../Adiabatic_Inversion/pulse.h"


using namespace std;


///---------------------------------------------
/// IO functions
///---------------------------------------------
double * read_double(const char* file_name, long *size);



///---------------------------------------------
/// main program
///---------------------------------------------
int main(){

  cout<<"Run simulate at:"<<endl;
  cout<<"   1) 1.5T"<<endl;
  cout<<"   2) 3.0T"<<endl;
  cout<<"   3) 7.0T"<<endl;
  cout<<"   4) 7.0T_PTX"<<endl;
  cout<<"Select option: "<<endl;

  int i_option = 0;
  bool bGet= true;
  while(bGet){
    cin >> i_option;
    for(int i=1; i<5; i++){
      if (i_option==i){
        bGet=false;
      }
    }

  }



  ///---------------------------------------------
  /// Load sequence files
  ///---------------------------------------------
  long   l_Size_Of_RFpulse;
  long   l_Size_Of_B1map;


  double* d_rfAmp      =   read_double("Adiabatic_Inversion/HBS_amp.double",&l_Size_Of_RFpulse);
  double* d_rfPha      =   read_double("Adiabatic_Inversion/HBS_pha.double",&l_Size_Of_RFpulse);
  double* d_B1map;

  switch(i_option){
    case  1:
      d_B1map  =   read_double("MODEL/CP_64Mhz.double",&l_Size_Of_B1map);
      break;
    case  2:
      d_B1map  =   read_double("MODEL/CP_128Mhz.double",&l_Size_Of_B1map);
      break;
    case  3:
      d_B1map  =   read_double("MODEL/CP_298Mhz.double",&l_Size_Of_B1map);
      break;
    case  4:
      d_B1map  =   read_double("MODEL/RF_298Mhz.double",&l_Size_Of_B1map);
    break;
  }

  double* d_Inversion  =   new double [l_Size_Of_B1map];

  pulse epg_rf_sim;

  epg_rf_sim.init(d_rfAmp,d_rfPha,l_Size_Of_RFpulse);

  ///---------------------------------------------
  /// simulate
  ///---------------------------------------------
  for(int i=0; i<l_Size_Of_B1map; i++){
    d_Inversion[i] = epg_rf_sim.sim(d_B1map[i]);
  }

  ///---------------------------------------------
  /// save
  ///---------------------------------------------
  ofstream myfile;

  switch(i_option){
    case  1:
      myfile.open ("MODEL/64MHz_HBS_inversion.double", ofstream::binary);
      break;
    case  2:
      myfile.open ("MODEL/128MHz_HBS_inversion.double", ofstream::binary);
      break;
    case  3:
      myfile.open ("MODEL/298MHz_HBS_inversion.double", ofstream::binary);
      break;
    case  4:
      myfile.open ("MODEL/298MHz_HBS_B1_sh_inv.double", ofstream::binary);
    break;
  }
  myfile.write(&reinterpret_cast<char&>(*d_Inversion),l_Size_Of_B1map*sizeof(double));
  myfile.close();

  delete[] d_rfAmp;
  delete[] d_rfPha;

  return 0;
}


double * read_double(const char* file_name, long *size){
  char  *binary_data1;
  ifstream myfile (file_name, ios::in|ios::binary|ios::ate);
  if(myfile.is_open()){

    long byte_size= (long) myfile.tellg();
    //    cout<<"size in bytes: "<<byte_size<<endl;
    binary_data1=new char[byte_size];
    myfile.seekg(0,ios::beg);
    myfile.read(binary_data1,byte_size);
    myfile.close();

    *size=byte_size/sizeof(double);
    double *data= &reinterpret_cast<double&>(*binary_data1);

    return data;
  }
  cout<<"---------------------------------------------"<<endl;
  cout<<"file not found "<< file_name <<endl;
  cout<<"---------------------------------------------"<<endl;

  exit(0);
  return NULL;
}
