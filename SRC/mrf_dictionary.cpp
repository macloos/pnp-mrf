////////////////////////////////////////////////////////////////////////////////
// Project Name :   MRF dictionary creator
// Discription  :   This program creates a dictionry for use in the MRF
//                  reconstruction process. Creates the equvilent onresonance
//                  part of the dictionary discribed in Ma. et al., Nature 2013.
//
//                  This dictionary was used for examlple 1 and 2 in:
//                  Cloos et al., Nature Communications 2016
//
// Author       :   Martijn Cloos
// Email: martijn.cloos@nyumc.org
////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <complex>
#include <pthread.h>
#include <cmath>


#ifdef MRFCONFIG
#include "../SEQ_MRF/mrf_data_base.h"
#include "../SEQ_MRF/mrf_config.h"
#else
#error must be compliled with -DMRFCONFIG
#endif

#include "../EPG_CODE/dualpop.h"
#include "utility.hpp"


///---------------------------------------------
/// main program
///---------------------------------------------
int main(){
  std::cout<<"===================================================="<<std::endl;
  std::cout<<"MRF as discribed by:          Ma et al., Nature 2013"<<std::endl;
  std::cout<<"Part of comparison package:          MRF vs. PnP-MRF"<<std::endl;
  std::cout<<"Cloos et al.,             Nature Communications 2016"<<std::endl;


  ///---------------------------------------------
  /// Variable for self consistency checking
  ///---------------------------------------------
  bool b_check = true;


  ///---------------------------------------------
  /// Load sequence file
  ///---------------------------------------------
  std::vector<double> * vd_sequence_amp =  new vector<double>[2];
  utility::load_bin("SEQ_MRF/MRF_FA.double", vd_sequence_amp[0]);
  utility::load_bin("SEQ_MRF/MRF_FA.double", vd_sequence_amp[1]);
  std::vector<double> * vd_sequence_pha =  new vector<double>[2];
  utility::load_bin("SEQ_MRF/MRF_PH.double", vd_sequence_pha[0]);
  utility::load_bin("SEQ_MRF/MRF_PH.double", vd_sequence_pha[1]);
  std::vector<double> vd_sequence_tr;
  utility::load_bin("SEQ_MRF/MRF_TR.double", vd_sequence_tr);


  ///---------------------------------------------
  /// Check sequence files
  ///---------------------------------------------
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_pha[0].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_tr.size()    ) b_check = false;
  if(vd_sequence_amp[0].size() != SIZE_OF_FINGERPRINT      ) b_check = false;
  if(!b_check){std::cout<<"Inconcistent sequence files."<<std::endl; exit(0);}


  ///---------------------------------------------
  /// FUBAR
  ///---------------------------------------------
  double** dRF_sequence  =   new double*[2];
  dRF_sequence[0]        =   &vd_sequence_amp[0][0];
  dRF_sequence[1]        =   &vd_sequence_amp[1][0];

  double** dPh_sequence  =   new double*[2];
  dPh_sequence[0]        =   &vd_sequence_pha[0][0];
  dPh_sequence[1]        =   &vd_sequence_pha[0][0];


  ///---------------------------------------------
  /// allocate memory for the results
  ///---------------------------------------------
  std::vector<SAVEFORMAT> vt_normalized_data(l_size_of_database_file * SIZE_OF_FINGERPRINT);
  std::vector<float>      vf_lookuptable    (l_size_of_database_file * 3);


  ///---------------------------------------------
  /// Create Extended Phase Graph Engine
  ///---------------------------------------------
  dualpop simulation;


  ///---------------------------------------------
  /// Loop over all dictionary entrees
  ///---------------------------------------------
  long index = 0;
  for(int T1=0; T1<size_of_T1_array; T1++){
    for(int T2=0; T2<size_of_T2_array; T2++){

      ///------------------------------------------------------
      /// Prep Sim
      ///------------------------------------------------------
      SAVEFORMAT *pf_nor_data_loc = &vt_normalized_data[index*SIZE_OF_FINGERPRINT];

      simulation.mrf_init(dRF_sequence, dPh_sequence,&vd_sequence_tr[0], 60.f, T1_array[T1], T2_array[T2]);
      simulation.mrf_sim(pf_nor_data_loc);

      ///---------------------------------------------
      /// normalize
      ///---------------------------------------------
      float norm=0;
      for(int i=0; i<SIZE_OF_FINGERPRINT; i++){
        norm+=abs(pf_nor_data_loc[i]*conj(pf_nor_data_loc[i]));
      }

      norm = sqrt(norm);

      for(int i=0; i<SIZE_OF_FINGERPRINT; i++){
        pf_nor_data_loc[i]/=norm;
      }


      ///------------------------------------------------------
      /// Add indexs to lookup table
      ///------------------------------------------------------
      vf_lookuptable[index*3  ] = T1_array[T1];
      vf_lookuptable[index*3+1] = T2_array[T2];
      vf_lookuptable[index*3+2] = norm;


      ///------------------------------------------------------
      /// update progress bar
      ///------------------------------------------------------
      utility::progress_bar(index*1000.0f/(l_size_of_database_file), 1000, 100, 10, "Creating MRF Dictionary:");


      ///--------------------------------------------------
      /// Next dictionary entree
      ///--------------------------------------------------
      index++;
    } //T2
  } //T1

  ///--------------------------------------------------
  /// Save Dictionary to disk
  ///--------------------------------------------------
  utility::save_bin("mrf_dictionary.float"  ,vt_normalized_data);
  utility::save_bin("mrf_lookup_table.float",vf_lookuptable    );


  std::cout<<"Creating MRF Dictionary:                    Finished"<<std::endl;
  std::cout<<"===================================================="<<std::endl;

  return 0;
}
