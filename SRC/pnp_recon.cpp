//==============================================================================
//==============================================================================
// Project Name : PnP-MRF reconstruction
// Language     : C++11 including AVX
// Discription  :
// This code is part of the example data + code set presented in
// Cloos et al., Nature Communications 2016.
//
// This program performs the exhaustive dictionary search needed
// to reconstruct an PnP-MRF measurment. Benchmarked on a
// macbook pro retina (mid 2012), Total reconstruction time ~294sec
//
// Author: Martijn Cloos
// Email : Martijn.Cloos@nyumc.org
//==============================================================================
//==============================================================================

#include <iostream>
#include <fstream>
#include <thread>
#include <vector>
#include <cmath>
#include <immintrin.h> //Intel Advanced Vector Extensions (AVX)

#include "../SEQ_PNP/lookup_table_header.hpp"
#include "utility.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::thread;

#define FINGERPRINT_SIZE   32
#define DICTI_CHUNK_SIZE 8192
#define MIN(a,b) (((a)<(b))?(a):(b))

struct threadInterface{
  float *pf_Dictionary;
  float *pf_MeasLoc;
  float *pf_MaskLoc;
  float *pf_MaxCoLoc;
  long  *pl_MatchLoc;

  long l_ImageSize;
  long l_DictionaryShift;
  long l_DictionaryChunk;
  int  i_NrThreads;
  int  i_CPUID;
};

///-----------------------------------------------------------------------------
/// IO functions
///-----------------------------------------------------------------------------
void load_float(const char* file_name, vector<float> &data);
void save_float(const char* file_name, vector<float> &data);
void show_progress_bar(int x, int n, int r, int w, const char * tag);
void slow_dot_product(float* left, float* right, float* res);


///-----------------------------------------------------------------------------
/// Recon functions
///-----------------------------------------------------------------------------
vector<long> find_dictionary_match(vector<float> &vf_Dictionary,
                                   vector<float> &vf_MeasData,
                                   vector<float> &vf_Mask);
void match(threadInterface thIO);
void translate(vector<float> &vf_Dictionary,
               vector<float> &vf_LookupTable,
               vector<float> &vf_MeasData,
               vector<float> &vf_Mask,
               vector<long>  &vl_Match);

///-----------------------------------------------------------------------------
/// Main
///-----------------------------------------------------------------------------
int main(){
  cout<<"===================================================="<<endl;
  cout<<"PnP-MRF: Cloos et al.,    Nature Communications 2015"<<endl;

  vector<float> vf_Dictionary;
  vector<float> vf_LookupTable;
  vector<float> vf_MeasData;
  vector<float> vf_Mask;

  pnp_mrf::load_lookup("pnp_lookup_table.bin" , vf_LookupTable );
  utility::load_bin("pnp_dictionary.float"    , vf_Dictionary  );
  utility::load_bin("pnp_acquisition.float"   , vf_MeasData    );
  utility::load_bin("MODEL/mask.float"        , vf_Mask        );

  auto t_start  = std::chrono::high_resolution_clock::now();
  auto vl_Match = find_dictionary_match(vf_Dictionary, vf_MeasData, vf_Mask);
  translate(vf_Dictionary, vf_LookupTable, vf_MeasData, vf_Mask, vl_Match);
  auto t_end    = std::chrono::high_resolution_clock::now();


  double d_total_time = (std::chrono::duration<double>(t_end-t_start).count());
  cout<<"PnP-MRF total recon time:              "<< d_total_time <<" sec"<< endl;
  cout<<"===================================================="<<endl;

  return 0;
}


//==============================================================================
// Function: find_dictionary_match
//
// Discription:
// This function idenitfies the dictionary element that best matches
// the measured fingerprints. The process is distributed over multiple
// concurent threads each searching fraction of the image space [match(thIO)]
// Because these dictionaries can be very large, the search is devided
// into chunks of [DICTI_CHUNK_SIZE]. This avoids a potential bottleneck
// fromed by the finite CPU cache size.
//==============================================================================
vector<long> find_dictionary_match(vector<float> &vf_Dictionary,
                                   vector<float> &vf_MeasData,
                                   vector<float> &vf_Mask){

  ///---------------------------------------------------------------------------
  /// Containers for the results
  ///---------------------------------------------------------------------------
  vector<float> vf_MaxCo(vf_Mask.size(),-1.0f);
  vector<long>  vl_Match(vf_Mask.size(), 0.0f);

  long   l_size_of_Dictionary = vf_Dictionary.size();
         l_size_of_Dictionary/= FINGERPRINT_SIZE;

  ///---------------------------------------------------------------------------
  /// Multi Thread interface
  ///---------------------------------------------------------------------------
  vector<thread>  th;
  threadInterface thInterface;
  unsigned int ui_MaxNrOfThreads = thread::hardware_concurrency();

  ///---------------------------------------------------------------------------
  /// Loop of over dictionary chuncks
  ///---------------------------------------------------------------------------
  long  l_Index=0;
  while(l_Index < l_size_of_Dictionary){

    utility::progress_bar(l_Index*1000.0f/(l_size_of_Dictionary), 1000, 100, 10, "PnP-MRF dictionary matching :   ");

    ///-------------------------------------------------------------------------
    /// Launch a group of threads
    ///-------------------------------------------------------------------------
    while(th.size()<ui_MaxNrOfThreads){

        thInterface.pf_Dictionary    = &vf_Dictionary[l_Index*FINGERPRINT_SIZE];
        thInterface.pf_MeasLoc       = &vf_MeasData[th.size()*FINGERPRINT_SIZE];
        thInterface.pf_MaskLoc       = &vf_Mask[th.size()];
        thInterface.pf_MaxCoLoc      = &vf_MaxCo[th.size()];
        thInterface.pl_MatchLoc      = &vl_Match[th.size()];

        thInterface.l_ImageSize      = vf_Mask.size();
        thInterface.l_DictionaryShift= l_Index;
        thInterface.l_DictionaryChunk= MIN(DICTI_CHUNK_SIZE, (long) (l_size_of_Dictionary - l_Index));

        thInterface.i_NrThreads      = ui_MaxNrOfThreads;
        thInterface.i_CPUID          = th.size();

        th.push_back(thread(match, thInterface));
    }

    ///-------------------------------------------------------------------------
    /// Join threads with the main
    ///-------------------------------------------------------------------------
    for(auto &t : th){
      t.join();
    }

    ///-------------------------------------------------------------------------
    /// Prepare for next chunk
    ///-------------------------------------------------------------------------
    th.clear();
    l_Index+=DICTI_CHUNK_SIZE;
  }

  return vl_Match;
}

//==============================================================================
// Function:    match(threadInterface thIO)
//
// Discription:
// This function idenitfies the dictionary element that best matches
// the measured fingerprints. It iterates through the image space with
// a step size [thIO.i_NrThreads]. The section encased in <AVX>> ... </AVX>
// implements the dot product using Intel's Advanced Vector Extensions (AVX).
//==============================================================================
void match(threadInterface thIO){

  float *pf_DLoc;
  float *pf_MLoc;

  long lJ;

  for(long lI = 0; lI<thIO.l_ImageSize; lI+=thIO.i_NrThreads){ //image space

    if(thIO.pf_MaskLoc[lI]>0.01f){
      pf_MLoc = &(thIO.pf_MeasLoc[lI*FINGERPRINT_SIZE]);

      __m256 avxL1 = _mm256_load_ps(pf_MLoc   );
      __m256 avxL2 = _mm256_load_ps(pf_MLoc+ 8);
      __m256 avxL3 = _mm256_load_ps(pf_MLoc+16);
      __m256 avxL4 = _mm256_load_ps(pf_MLoc+24);

      for(lJ=0; lJ<thIO.l_DictionaryChunk; ++lJ){
        pf_DLoc = &thIO.pf_Dictionary[lJ*FINGERPRINT_SIZE];

        __m256 avxR1 = _mm256_load_ps(pf_DLoc   );
        __m256 avxR2 = _mm256_load_ps(pf_DLoc+ 8);
        __m256 avxR3 = _mm256_load_ps(pf_DLoc+16);
        __m256 avxR4 = _mm256_load_ps(pf_DLoc+24);

        //<AVX>-----------------------------------------------------------------
        __m256 avxT1 = _mm256_add_ps(_mm256_add_ps(_mm256_add_ps(
                       _mm256_dp_ps(avxL1, avxR1, 0xff),
                       _mm256_dp_ps(avxL2, avxR2, 0xff)),
                       _mm256_dp_ps(avxL3, avxR3, 0xff)),
                       _mm256_dp_ps(avxL4, avxR4, 0xff));

        __m256 pf_tmp = _mm256_add_ps(avxT1, _mm256_permute2f128_ps(avxT1, avxT1, 1));
        //</AVX>----------------------------------------------------------------

#ifdef __GNUC__
        if(pf_tmp[0] > thIO.pf_MaxCoLoc[lI]){
          thIO.pl_MatchLoc[lI] = thIO.l_DictionaryShift+lJ;
          thIO.pf_MaxCoLoc[lI] = pf_tmp[0];
        }
#else
        if(pf_tmp.m256_f32[0] > thIO.pf_MaxCoLoc[lI]){
          thIO.pl_MatchLoc[lI] = thIO.l_DictionaryShift+lJ;
          thIO.pf_MaxCoLoc[lI] = pf_tmp.m256_f32[0];
        }
#endif

      } //dictionary chunck
    } //mask
  } //image space
}


//==============================================================================
// Function:    translate(threadInterface thIO)
// Discription:
// This function idenitfies the dictionary element that best matches
// the measured fingerprints. It iterates through the image space with
// a step size [thIO.i_NrThreads]. The section encased in <AVX>> ... </AVX>
// implements the dot product using Intel's Advanced Vector Extensions (AVX).
//==============================================================================
void translate(vector<float> &vf_Dictionary,
               vector<float> &vf_LookupTable,
               vector<float> &vf_MeasData,
               vector<float> &vf_Mask,
               vector<long>  &vl_Match){

  vector<float> vf_L1map(vl_Match.size(), 0.0f);
  vector<float> vf_L2map(vl_Match.size(), 0.0f);
  vector<float> vf_T1map(vl_Match.size(), 0.0f);
  vector<float> vf_T2map(vl_Match.size(), 0.0f);
  vector<float> vf_PDmap(vl_Match.size(), 0.0f);

  for(int i=0; i<vf_Mask.size(); i++){
    utility::progress_bar(i*1000.0f/vf_Mask.size(), 1000, 100, 10, "PnP-MRF reconstruction progress:");

    if(vf_Mask[i]>0.001){

      slow_dot_product(&vf_MeasData[i*FINGERPRINT_SIZE],
                  &vf_MeasData[i*FINGERPRINT_SIZE],
                  &vf_PDmap[i]);

      vf_PDmap[i] = std::sqrt(vf_PDmap[i]);

      vf_L1map[i] = vf_LookupTable[vl_Match[i]*5+4];
      vf_L2map[i] = vf_LookupTable[vl_Match[i]*5+3];
      vf_T1map[i] = vf_LookupTable[vl_Match[i]*5+2];
      vf_T2map[i] = vf_LookupTable[vl_Match[i]*5+1];
      vf_PDmap[i]/= vf_LookupTable[vl_Match[i]*5+0];

    }
  }

  float f_norm = -1.0f;
  for(int i=0; i<vf_PDmap.size(); i++){
    if(vf_PDmap[i] > f_norm){
      f_norm = vf_PDmap[i];
    }
  }

  for(int i=0; i<vf_PDmap.size(); i++){
     vf_PDmap[i]/=f_norm;
  }

  utility::save_bin("Results/PNP_recon_T1.float", vf_T1map);
  utility::save_bin("Results/PNP_recon_T2.float", vf_T2map);
  utility::save_bin("Results/PNP_recon_PD.float", vf_PDmap);
  utility::save_bin("Results/PNP_recon_M1.float", vf_L1map);
  utility::save_bin("Results/PNP_recon_M2.float", vf_L2map);
}

///-----------------------------------------------------------------------------
/// Slow dot product (simple)
///-----------------------------------------------------------------------------
void slow_dot_product(float* left, float* right, float *res){
  *res = 0.0f;
  for(int i=0; i<FINGERPRINT_SIZE; i++){
    *res += left[i]*right[i];
  }
}
