////////////////////////////////////////////////////////////////////////////////
// Project Name :   MRF dictionary creator
// Discription  :   This program creates a dictionry for use in the PnP-MRF
//                  reconstruction process. Creates the equvilent dictionary
//                  discribed in Cloos et al., Nature Communications 2016.
//
// Author       :   Martijn Cloos
// Email: martijn.cloos@nyumc.org
////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <complex>
#include <thread>
#include <vector>
#include <cmath>
#include <cstring>

#ifdef MRFCONFIG
  #undef MRFCONFIG
#endif

#ifdef PNPCONFIG
  #include "../SEQ_PNP/pnp_data_base.h"
  #include "../SEQ_PNP/pnp_config.h"
  #include "../SEQ_PNP/lookup_table_header.hpp"
#else
  #error must be compliled with -DPNPCONFIG
#endif

#include "../EPG_CODE/dualpop.h"
#include "utility.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::thread;
using std::strcpy

using pnp_mrf::lookup_table_header;

///---------------------------------------------
/// IO functions
///---------------------------------------------
lookup_table_header  create_header(float f_TR, float f_TBW);


///---------------------------------------------
/// Multi Thread Kernel
///---------------------------------------------
void run_epg(long l_index);


///---------------------------------------------
/// Multi Thread interface
///---------------------------------------------
unsigned int ui_MaxNrOfThreads = thread::hardware_concurrency();
static long size_of_compressed_fingerprint =  SIZE_OF_FINGERPRINT/COMPRESSION_FACTOR;

dualpop* simulation          = new dualpop[ui_MaxNrOfThreads];
SAVEFORMAT **pf_nor_data_loc = new SAVEFORMAT *[ui_MaxNrOfThreads];
SAVEFORMAT **pf_tmp_data_loc = new SAVEFORMAT *[ui_MaxNrOfThreads];

SAVEFORMAT  *nor_data = NULL;
SAVEFORMAT  *tmp_data = NULL;

float **pf_nor_fact_loc = new float *[ui_MaxNrOfThreads];
float  *dim_data        = NULL;


///---------------------------------------------
/// main program
///---------------------------------------------
int main(){
  cout<<"===================================================="<<endl;
  cout<<"PnP-MRF: Cloos et al.,    Nature Communications 2016"<<endl;


  ///---------------------------------------------
  /// Variable for self consistency checking
  ///---------------------------------------------
  bool b_check = true;


  ///---------------------------------------------
  /// Load sequence file
  ///---------------------------------------------
  std::vector<double> * vd_sequence_amp =  new vector<double>[2];
  utility::load_bin("SEQ_PNP/TX1_amp.double", vd_sequence_amp[0]);
  utility::load_bin("SEQ_PNP/TX2_amp.double", vd_sequence_amp[1]);
  std::vector<double> * vd_sequence_pha =  new vector<double>[2];
  utility::load_bin("SEQ_PNP/TX1_pha.double", vd_sequence_pha[0]);
  utility::load_bin("SEQ_PNP/TX2_pha.double", vd_sequence_pha[1]);
  std::vector<double> vd_sequence_tr;
  utility::load_bin("SEQ_PNP/TR.double", vd_sequence_tr);


  ///---------------------------------------------
  /// Check sequence files
  ///---------------------------------------------
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_pha[0].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_tr.size()    ) b_check = false;
  if(vd_sequence_amp[0].size() != SIZE_OF_FINGERPRINT      ) b_check = false;
  if(!b_check){std::cout<<"Inconcistent sequence files."<<std::endl; exit(0);}


  ///---------------------------------------------
  /// FUBAR
  ///---------------------------------------------
  double** dRF_sequence  =   new double*[2];
  dRF_sequence[0]        =   &vd_sequence_amp[0][0];
  dRF_sequence[1]        =   &vd_sequence_amp[1][0];

  double** dPh_sequence  =   new double*[2];
  dPh_sequence[0]        =   &vd_sequence_pha[0][0];
  dPh_sequence[1]        =   &vd_sequence_pha[0][0];


  ///---------------------------------------------
  /// Allocate memory for the results
  ///---------------------------------------------
  tmp_data =new (std::nothrow) SAVEFORMAT[ui_MaxNrOfThreads * SIZE_OF_FINGERPRINT];
  nor_data =new (std::nothrow) SAVEFORMAT[l_size_of_database_file * size_of_compressed_fingerprint];
  dim_data =new (std::nothrow) float[l_size_of_database_file * 5];
  if(!tmp_data || !nor_data || !dim_data){
    cout<<"unable to allcocate memory"<<endl;
    exit(0);
  }

  for(int i=0; i<ui_MaxNrOfThreads; i++){
    pf_tmp_data_loc[i]= &tmp_data[i*SIZE_OF_FINGERPRINT];
  }

  ///---------------------------------------------------------------------------
  /// Multi Thread interface
  ///---------------------------------------------------------------------------
  vector<thread>  th;
  ///---------------------------------------------------------------------------
  /// Loop of over all dictionary entrees
  ///---------------------------------------------------------------------------
  long l_index = 0;

  for(int B1=0; B1<l_B1m; B1++){
    for(int B2=0; B2<l_B2m; B2++){
      for(int T1=0; T1<l_T1m; T1++){
        for(int T2=0; T2<l_T2m; T2++){

          ///---------------------------------------------------------------------------
          /// Run Batch
          ///---------------------------------------------------------------------------
          if(th.size()==ui_MaxNrOfThreads){
            for(auto &t : th){
              t.join();
            }
            th.clear();
          }

          ///------------------------------------------------------
          /// Add indexs to lookup table
          ///------------------------------------------------------
          dim_data[l_index * 5 + 4] = TX1_array[B1];
          dim_data[l_index * 5 + 3] = TX2_array[B2];
          dim_data[l_index * 5 + 2] = T1_array[T1];
          dim_data[l_index * 5 + 1] = T2_array[T2];

          ///---------------------------------------------------------------------------
          /// Prepare Batch
          ///---------------------------------------------------------------------------
          pf_nor_data_loc[th.size()]= &nor_data[l_index * size_of_compressed_fingerprint];
          pf_nor_fact_loc[th.size()]= &dim_data[l_index * 5];

          if(l_B2m==1){
            simulation[th.size()].pnp_init(dRF_sequence, dPh_sequence, &vd_sequence_tr[0], TX1_array[B1], TX1_array[B1], T1_array[T1], T2_array[T2],1.0f);
          }else{
            simulation[th.size()].pnp_init(dRF_sequence, dPh_sequence, &vd_sequence_tr[0], TX1_array[B1], TX2_array[B2], T1_array[T1], T2_array[T2],1.0f);
          }

          th.push_back(thread(run_epg, th.size()));

          ///------------------------------------------------------
          /// update progress bar
          ///------------------------------------------------------
          utility::progress_bar(l_index*1000.0f/(l_size_of_database_file), 1000, 100, 10, "Creating PnP-MRF Dictionary: ");

          l_index++;
        }
      }
    }
  }


  ///---------------------------------------------------------------------------
  /// Run Final Batch
  ///---------------------------------------------------------------------------
  for(auto &t : th){
    t.join();
  }
  th.clear();



  ///---------------------------------------------
  /// Save Data to disk
  ///---------------------------------------------
  lookup_table_header header;
  if(SPAT_STEPS == 1){
    header = create_header(vd_sequence_tr[0],100.0f);
  }else{
    header = create_header(vd_sequence_tr[0],  3.0f);
  }

  std::ofstream myfile;
  myfile.open ("pnp_lookup_table.bin", std::ofstream::binary);
  myfile.write((char*)&header, sizeof(header));
  myfile.write(&reinterpret_cast<char&>(*dim_data),l_size_of_database_file * 5 * sizeof(float));
  myfile.close();

  myfile.open ("pnp_dictionary.float", std::ofstream::binary);
  myfile.write(&reinterpret_cast<char&>(*nor_data),l_size_of_database_file*size_of_compressed_fingerprint*sizeof(SAVEFORMAT));
  myfile.close();

  cout<<"Creating PnP-MRF Dictionary:                Finished"<<endl;
  cout<<"===================================================="<<endl;

  delete[] nor_data;
  delete[] tmp_data;
  delete[] dim_data;

  return 0;
}




void run_epg(long l_id){
  simulation[l_id].pnp_sim(pf_tmp_data_loc[l_id]);


  if(COMPRESSION_FACTOR>1){
  ///---------------------------------------------
  /// Compress 1st FLASH segment
  ///---------------------------------------------
  for(int i=0; i<8; i+=2){
    pf_nor_data_loc[l_id][i  ] = 0.f;
    pf_nor_data_loc[l_id][i+1] = 0.f;

    for(int j=0; j<2*COMPRESSION_FACTOR; j+=2){
      pf_nor_data_loc[l_id][i  ] += pf_tmp_data_loc[l_id][i*COMPRESSION_FACTOR + j  ];
      pf_nor_data_loc[l_id][i+1] += pf_tmp_data_loc[l_id][i*COMPRESSION_FACTOR + j+1];
    } // j
  }


  ///---------------------------------------------
  /// Compress 1st FISP segment
  ///---------------------------------------------
  for(int i=8; i<16; i++){
    pf_nor_data_loc[l_id][i  ] = 0.f;
    for(int j=0; j<COMPRESSION_FACTOR; j++){
      pf_nor_data_loc[l_id][i] += pf_tmp_data_loc[l_id][i*COMPRESSION_FACTOR + j];
    } // j
  }


  ///---------------------------------------------
  /// Compress 2nd FLASH segment
  ///---------------------------------------------
  for(int i=16; i<24; i+=2){
    pf_nor_data_loc[l_id][i  ] = 0.f;
    pf_nor_data_loc[l_id][i+1] = 0.f;

    for(int j=0; j<2*COMPRESSION_FACTOR; j+=2){
      pf_nor_data_loc[l_id][i  ] += pf_tmp_data_loc[l_id][i*COMPRESSION_FACTOR + j  ];
      pf_nor_data_loc[l_id][i+1] += pf_tmp_data_loc[l_id][i*COMPRESSION_FACTOR + j+1];
    } // j
  }


  ///---------------------------------------------
  /// Compress 2nd FISP segment
  ///---------------------------------------------
  for(int i=24; i<32; i++){
    pf_nor_data_loc[l_id][i  ] = 0.f;
    for(int j=0; j<COMPRESSION_FACTOR; j++){
      pf_nor_data_loc[l_id][i] += pf_tmp_data_loc[l_id][i*COMPRESSION_FACTOR + j];
    } // j
  }
}else{
  for(int i=0; i<size_of_compressed_fingerprint; i++){
      pf_nor_data_loc[l_id][i] += pf_tmp_data_loc[l_id][i];
  }
}


  ///---------------------------------------------
  /// normalize
  ///---------------------------------------------
  float norm=0;
  for(int i=0; i<size_of_compressed_fingerprint; i++){
      norm+=abs(pf_nor_data_loc[l_id][i]*conj(pf_nor_data_loc[l_id][i]));
  }

  norm = sqrt(norm);

  for(int i=0; i<size_of_compressed_fingerprint; i++){
        pf_nor_data_loc[l_id][i]/=norm;
  }


  pf_nor_fact_loc[l_id][0] = norm;

}



///---------------------------------------------
/// create dictionary header
///---------------------------------------------
lookup_table_header create_header(float f_TR, float f_TBW){
  lookup_table_header header;

  header.l_size_of_database_file = l_T2m*l_T1m*l_B1m*l_B2m;
  header.l_Dim = 5;
  header.f_TR  = f_TR;
  header.f_TBW = f_TBW;
  header.l_size_of_compressed_fingerprint = SIZE_OF_FINGERPRINT/COMPRESSION_FACTOR;

  header.l_T2m = l_T2m;
  header.l_T1m = l_T1m;
  header.l_B2m = l_B2m;
  header.l_B1m = l_B1m;

  header.f_min_PD =  0.f;
  header.f_max_PD = 23.f;
  header.f_min_T2 = T2_array[    0];
  header.f_max_T2 = T2_array[l_T2m];
  header.f_min_T1 = T1_array[    0];
  header.f_max_T1 = T1_array[l_T1m];
  header.f_min_B1 = TX1_array[    0];
  header.f_max_B1 = TX1_array[l_B1m];
  header.f_min_B2 = TX2_array[    0];
  header.f_max_B2 = TX2_array[l_B2m];

  header.i_size_of_label_PD = 2;
  header.i_size_of_label_T2 = 2;
  header.i_size_of_label_T1 = 2;
  header.i_size_of_label_B2 = 6;
  header.i_size_of_label_B1 = 6;

  strcpy(header.c_label_PD, "PD");
  strcpy(header.c_label_T2, "T2");
  strcpy(header.c_label_T1, "T1");
  strcpy(header.c_label_B2, "B1_TX2");
  strcpy(header.c_label_B1, "B1_TX1");

  return header;
}
