////////////////////////////////////////////////////////////////////////////////
// Project Name :   PnP-MRF aquisition simulator
// Discription  :   This program creates a simulated PnP-MRF dataset,
//                  which corresponds to the 3rd example shown of
//                  Cloos et al., Nature Communications 2016
//
//                  This program can also be used to create dictionaries
//                  for the reconstruction of actual scanner data.
//                  To this end, the slice profile must be encorporated
//                  (see SEQ_PNP/pnp_config.h)
//
// Author       :   Martijn Cloos
// Email: martijn.cloos@nyumc.org
////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <complex>
#include <cmath>

#ifdef PNPCONFIG
  #include "../SEQ_PNP/pnp_config.h"
#else
  #error must be compliled with -DPNPCONFIG
#endif

#include "../EPG_CODE/dualpop.h"
#include "utility.hpp"


///---------------------------------------------
/// main program
///---------------------------------------------
int main(){
  std::cout<<"===================================================="<<std::endl;
  std::cout<<"PnP-MRF: Cloos et al.,    Nature Communications 2016"<<std::endl;


  ///---------------------------------------------
  /// Variable for self consistency checking
  ///---------------------------------------------
  bool b_check = true;


  ///---------------------------------------------
  /// Load sequence file
  ///---------------------------------------------
  std::vector<double> * vd_sequence_amp =  new vector<double>[2];
  utility::load_bin("SEQ_PNP/TX1_amp.double", vd_sequence_amp[0]);
  utility::load_bin("SEQ_PNP/TX2_amp.double", vd_sequence_amp[1]);
  std::vector<double> * vd_sequence_pha =  new vector<double>[2];
  utility::load_bin("SEQ_PNP/TX1_pha.double", vd_sequence_pha[0]);
  utility::load_bin("SEQ_PNP/TX2_pha.double", vd_sequence_pha[1]);
  std::vector<double> vd_sequence_tr;
  utility::load_bin("SEQ_PNP/TR.double", vd_sequence_tr);


  ///---------------------------------------------
  /// Check sequence files
  ///---------------------------------------------
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_pha[0].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_tr.size()    ) b_check = false;
  if(vd_sequence_amp[0].size() != SIZE_OF_FINGERPRINT      ) b_check = false;
  if(!b_check){std::cout<<"Inconcistent sequence files."<<std::endl; exit(0);}


  ///---------------------------------------------
  /// FUBAR
  ///---------------------------------------------
  double** dRF_sequence  =   new double*[2];
  dRF_sequence[0]        =   &vd_sequence_amp[0][0];
  dRF_sequence[1]        =   &vd_sequence_amp[1][0];

  double** dPh_sequence  =   new double*[2];
  dPh_sequence[0]        =   &vd_sequence_pha[0][0];
  dPh_sequence[1]        =   &vd_sequence_pha[0][0];


  ///---------------------------------------------
  /// Load model files
  ///---------------------------------------------
  std::vector<double> vd_model_T1; //Longitudinal relaxation time in  [ms]
  std::vector<double> vd_model_T2; //Transverse relaxation time in    [ms]
  std::vector<double> vd_model_PD; //Proton Density                   [a.u.]
  std::vector<double> vd_model_B1; //Transmit sensativity channel 1   [a.u.]
  std::vector<double> vd_model_B2; //Transmit sensativity channel 2   [a.u.]
  utility::load_bin("MODEL/T1_map.double"   , vd_model_T1);
  utility::load_bin("MODEL/T2_map.double"   , vd_model_T2);
  utility::load_bin("MODEL/PD_map.double"   , vd_model_PD);
  utility::load_bin("MODEL/M1_298MHz.double", vd_model_B1);
  utility::load_bin("MODEL/M2_298MHz.double", vd_model_B2);


  ///---------------------------------------------
  /// Check model files
  ///---------------------------------------------
  if(vd_model_T1.size() != vd_model_T2.size()) b_check = false;
  if(vd_model_T1.size() != vd_model_PD.size()) b_check = false;
  if(vd_model_T1.size() != vd_model_B1.size()) b_check = false;
  if(vd_model_T1.size() != vd_model_B2.size()) b_check = false;
  if(!b_check){std::cout<<"Inconcistent model files."<<std::endl; exit(0);}


  ///---------------------------------------------
  /// allocate memory for the results
  ///---------------------------------------------
  std::vector<SAVEFORMAT> v_normalized_data(vd_model_T1.size() * SIZE_OF_COMPRESSED_FINGERPRINT);
  std::vector<SAVEFORMAT> v_data           (SIZE_OF_FINGERPRINT);


  ///---------------------------------------------
  /// Create Extended Phase Graph Engine
  ///---------------------------------------------
  dualpop simulation;


  ///---------------------------------------------
  /// Loop over image space
  ///---------------------------------------------
  for(int ii=0; ii<vd_model_T1.size(); ii++){

    ///------------------------------------------------------
    /// update progress bar
    ///------------------------------------------------------
    utility::progress_bar(ii*1000.0f/(vd_model_T1.size()), 1000, 100, 10, "PnP-MRF Simulations progress:");


    ///------------------------------------------------------
    /// Run simulation for the iith voxel
    ///------------------------------------------------------
    simulation.pnp_init(dRF_sequence, dPh_sequence, &vd_sequence_tr[0], vd_model_B1[ii], vd_model_B2[ii], vd_model_T1[ii], vd_model_T2[ii], vd_model_PD[ii] );
    simulation.pnp_sim (&v_data[0]);


    ///---------------------------------------------
    /// Compress 1st FLASH segment
    ///---------------------------------------------
    SAVEFORMAT *p_loc = &v_normalized_data[ii * SIZE_OF_COMPRESSED_FINGERPRINT];
    if(COMPRESSION_FACTOR>1){
    for(int i=0; i<8; i+=2){
      p_loc[i  ] = 0.f;
      p_loc[i+1] = 0.f;

      for(int j=0; j<2*COMPRESSION_FACTOR; j+=2){
        p_loc[i  ] += v_data[i*COMPRESSION_FACTOR + j  ];
        p_loc[i+1] += v_data[i*COMPRESSION_FACTOR + j+1];
      } // j
    } // i

    ///---------------------------------------------
    /// Compress 1st FISP segment
    ///---------------------------------------------
    for(int i=8; i<16; i++){
      p_loc[i] = 0.f;
      for(int j=0; j<COMPRESSION_FACTOR; j++){
        p_loc[i] += v_data[i*COMPRESSION_FACTOR + j];
      } // j
    } // i

    ///---------------------------------------------
    /// Compress 2nd FLASH segment
    ///---------------------------------------------
    for(int i=16; i<24; i+=2){
      p_loc[i  ] = 0.f;
      p_loc[i+1] = 0.f;

      for(int j=0; j<2*COMPRESSION_FACTOR; j+=2){
        p_loc[i  ] += v_data[i*COMPRESSION_FACTOR + j  ];
        p_loc[i+1] += v_data[i*COMPRESSION_FACTOR + j+1];
      } // j
    } // i

    ///---------------------------------------------
    /// Compress 2nd FISP segment
    ///---------------------------------------------
    for(int i=24; i<32; i++){
      p_loc[i  ] = 0.f;
      for(int j=0; j<COMPRESSION_FACTOR; j++){
        p_loc[i] += v_data[i*COMPRESSION_FACTOR + j];
      } // j
    } // i
  }else{

    for(int i=0; i<SIZE_OF_FINGERPRINT; i++){
      p_loc[i] = v_data[i];

    } // i
  }

    ///---------------------------------------------
    /// Next voxel
    ///---------------------------------------------
  } // ii

  ///---------------------------------------------
  /// Save simulated PnP-MRF data set
  ///---------------------------------------------
  utility::save_bin("pnp_acquisition.float", v_normalized_data);

  std::cout<<"PnP-MRF Simulations progress:               Finished"<<std::endl;
  std::cout<<"===================================================="<<std::endl;

  return 0;
}
