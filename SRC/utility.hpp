#ifndef __UTILITY_HPP__
#define __UTILITY_HPP__

#include <vector>
#include <iostream>

namespace utility
{
  template<typename T>
  void load_bin(const char* file_name, std::vector<T> &data){
      std::ifstream input( file_name, std::ios::binary );

      if(input.is_open()){
        #ifdef __GNUC__
          std::cout << "Loading: " << file_name << " \n\033[F\033[J";
        #else
          std::cout << "Loading: " << file_name << " \b\r";
        #endif

        data.clear();
        input.seekg(0, input.end);
        data.reserve(input.tellg() / sizeof(T));
        input.seekg(0, input.beg);

        T buffer;
        while(input.read((char *)&buffer, sizeof(T))){
          data.push_back(buffer);
        }
        input.close();
      }else{
        std::cout << "Failed to load: " << file_name << std::endl;
        exit(0);
      }
  }

  template<typename T>
  void save_bin(const char* file_name, std::vector<T> &data){
    std::ofstream output( file_name, std::ofstream::binary );

    if(output.is_open()){
      output.write(&reinterpret_cast<char&>(data[0]), data.size() * sizeof(T));
      output.close();
    }else{
      std::cout << "Failed to save: " << file_name << std::endl;
      exit(0);
    }
  }

  ///-----------------------------------------------------------------------------
  /// IO function: show progress Bar
  ///-----------------------------------------------------------------------------
  void progress_bar(int x, int n, int r, int w, const char * tag)
  {
    if(n>r){ // Only update r times.
      if ( x % (n/r) != 0 ) return;
    }

    // Calculuate the ratio of complete-to-incomplete.
    float ratio = x/(float)n;
    int   c     = ratio * w;

    // Lable
    printf("%s ",tag);
    // Show the promille complete.
    printf("%4d%% [", (int)(ratio*100) );

    // Show the load bar.
    for (int x=0; x<c; x++)
    printf("=");

    for (int x=c; x<w; x++)
    printf(" ");

    // ANSI Control codes to go back to the previous line and clear it.
    #ifdef __GNUC__
      printf("]\n\033[F\033[J");
    #else
      printf("]\b\r");
    #endif
  }

} //namespace utility

#endif
