////////////////////////////////////////////////////////////////////////////////
// Project Name :   MRF simulator
// Discription  :   This program creates a simulated MRF dataset,
//                  which corresponds to the 1st and 2nd example shown of
//                  Cloos et al., Nature Communications 2016
//
// Author       :   Martijn Cloos
// Email: martijn.cloos@nyumc.org
////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <complex>
#include <cmath>


#ifdef MRFCONFIG
#include "../SEQ_MRF/mrf_config.h"
#else
#error must be compliled with -DMRFCONFIG
#endif

#include "../EPG_CODE/dualpop.h"
#include "utility.hpp"

using namespace std;


///---------------------------------------------
/// main program
///---------------------------------------------
int main(){
  cout<<"===================================================="<<endl;
  cout<<"MRF as discribed by:          Ma et al., Nature 2013"<<endl;
  cout<<"Part of comparison package:          MRF vs. PnP-MRF"<<endl;
  cout<<"Cloos et al.,             Nature Communications 2016"<<endl;

  ///---------------------------------------------
  /// Pick an experiment to run
  ///---------------------------------------------
  std::cout<<"Run simulate at:" <<std::endl;
  std::cout<<"   1) 1.5T"       <<std::endl;
  std::cout<<"   2) 3.0T"       <<std::endl;
  std::cout<<"   3) 7.0T"       <<std::endl;
  std::cout<<"   4) 7.0T_PTX"   <<std::endl;
  std::cout<<"Select option: ";

  int i_option = 0;
  bool bGet= true;
  while(bGet){
    std:cin >> i_option;
    for(int i=1; i<5; i++){
      if (i_option==i){
        bGet=false;
      }
    }

  }

  ///---------------------------------------------
  /// Variable for self consistency checking
  ///---------------------------------------------
  bool b_check = true;


  ///---------------------------------------------
  /// Load sequence file
  ///---------------------------------------------
  std::vector<double> * vd_sequence_amp =  new vector<double>[2];
  utility::load_bin("SEQ_MRF/MRF_FA.double", vd_sequence_amp[0]);
  utility::load_bin("SEQ_MRF/MRF_FA.double", vd_sequence_amp[1]);
  std::vector<double> * vd_sequence_pha =  new vector<double>[2];
  utility::load_bin("SEQ_MRF/MRF_PH.double", vd_sequence_pha[0]);
  utility::load_bin("SEQ_MRF/MRF_PH.double", vd_sequence_pha[1]);
  std::vector<double> vd_sequence_tr;
  utility::load_bin("SEQ_MRF/MRF_TR.double", vd_sequence_tr);


  ///---------------------------------------------
  /// Check sequence files
  ///---------------------------------------------
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_pha[0].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_amp[1].size()) b_check = false;
  if(vd_sequence_amp[0].size() != vd_sequence_tr.size()    ) b_check = false;
  if(vd_sequence_amp[0].size() != SIZE_OF_FINGERPRINT      ) b_check = false;
  if(!b_check){std::cout<<"Inconcistent sequence files."<<std::endl; exit(0);}


  ///---------------------------------------------
  /// FUBAR
  ///---------------------------------------------
  double** dRF_sequence  =   new double*[2];
  dRF_sequence[0]        =   &vd_sequence_amp[0][0];
  dRF_sequence[1]        =   &vd_sequence_amp[1][0];

  double** dPh_sequence  =   new double*[2];
  dPh_sequence[0]        =   &vd_sequence_pha[0][0];
  dPh_sequence[1]        =   &vd_sequence_pha[0][0];


  ///---------------------------------------------
  /// Load model files
  ///---------------------------------------------
  std::vector<double> vd_model_T1;
  std::vector<double> vd_model_T2;
  std::vector<double> vd_model_PD;
  std::vector<double> vd_model_B1;
  std::vector<double> vd_model_In;
  utility::load_bin("MODEL/T1_map.double", vd_model_T1);
  utility::load_bin("MODEL/T2_map.double", vd_model_T2);
  utility::load_bin("MODEL/PD_map.double", vd_model_PD);

  switch(i_option){
    case  1:
     utility::load_bin("MODEL/CP_64Mhz.double",            vd_model_B1);
     utility::load_bin("MODEL/64MHz_HBS_inversion.double", vd_model_In);
      break;
    case  2:
     utility::load_bin("MODEL/CP_128Mhz.double",            vd_model_B1);
     utility::load_bin("MODEL/128MHz_HBS_inversion.double", vd_model_In);
      break;
    case  3:
     utility::load_bin("MODEL/CP_298Mhz.double",            vd_model_B1);
     utility::load_bin("MODEL/298MHz_HBS_inversion.double", vd_model_In);
      break;
    case  4:
     utility::load_bin("MODEL/RF_298Mhz.double",            vd_model_B1);
     utility::load_bin("MODEL/298MHz_HBS_B1_sh_inv.double", vd_model_In);
      break;
  }


  ///---------------------------------------------
  /// Check model files
  ///---------------------------------------------
  if(vd_model_T1.size() != vd_model_T2.size()) b_check = false;
  if(vd_model_T1.size() != vd_model_PD.size()) b_check = false;
  if(vd_model_T1.size() != vd_model_B1.size()) b_check = false;
  if(vd_model_T1.size() != vd_model_In.size()) b_check = false;
  if(!b_check){std::cout<<"Inconcistent model files."<<std::endl; exit(0);}


  ///---------------------------------------------
  /// allocate memory for the results
  ///---------------------------------------------
  std::vector<SAVEFORMAT> vt_data(vd_model_T1.size() * SIZE_OF_FINGERPRINT);


  ///---------------------------------------------
  /// Create Extended Phase Graph Engine
  ///---------------------------------------------
  dualpop simulation;


  ///---------------------------------------------
  /// Loop over image space
  ///---------------------------------------------
  for(int ii=0; ii<vd_model_T1.size(); ii++){
    ///------------------------------------------------------
    /// Prep Sim
    ///------------------------------------------------------
    simulation.mrf_init(dRF_sequence, dPh_sequence, &vd_sequence_tr[0], vd_model_B1[ii], vd_model_T1[ii], vd_model_T2[ii], vd_model_PD[ii], vd_model_In[ii]);
    simulation.mrf_sim( &vt_data[ii*SIZE_OF_FINGERPRINT]);

    ///---------------------------------------------
    /// Show progress
    ///---------------------------------------------
    utility::progress_bar(ii*1000.0f/vd_model_T1.size(), 1000, 100, 10, "MRF Simulations progress:");
  }


  ///---------------------------------------------
  /// Save data
  ///---------------------------------------------
  switch(i_option){
    case  1:
      utility::save_bin("CP_64Mhz_mrf_acquisition.float", vt_data);
     break;
    case  2:
      utility::save_bin("CP_128Mhz_mrf_acquisition.float", vt_data);
      break;
    case  3:
      utility::save_bin("CP_298Mhz_mrf_acquisition.float", vt_data);
      break;
    case  4:
      utility::save_bin("RF_298Mhz_mrf_acquisition.float", vt_data);
    break;
  }

  std::cout<<"MRF Simulations progress:                   Finished"<<std::endl;
  std::cout<<"===================================================="<<std::endl;

  return 0;
}
