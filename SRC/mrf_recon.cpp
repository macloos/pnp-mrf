////////////////////////////////////////////////////////////////////////////////
// Project Name :   MRF reconstruction
// Discription  :   This program performs the dictionary matching based
//                  reconstruction of an MRF dataset as described by
//                  Ma et al., Nature 2013.
//
//                  This program can be used to reproduce examlple 1 and 2 in:
//                  Cloos et al., Nature Communications 2016
//
// Author       :   Martijn Cloos
// Email: martijn.cloos@nyumc.org
////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <complex>
#include <cmath>


#ifdef MRFCONFIG
  #include "../SEQ_MRF/mrf_config.h"
#else
  #error -DMRFCONFIH flag needed
#endif

#include "utility.hpp"


///---------------------------------------------
/// Dot product
///---------------------------------------------
void  dot_product(float* left, float* right, float* res);


///---------------------------------------------
/// main program
///---------------------------------------------
int main(){
  std::cout<<"===================================================="<<std::endl;
  std::cout<<"MRF as discribed by:          Ma et al., Nature 2013"<<std::endl;
  std::cout<<"Part of comparison package:          MRF vs. PnP-MRF"<<std::endl;
  std::cout<<"Cloos et al.,             Nature Communications 2016"<<std::endl;


  ///---------------------------------------------
  /// Pick an experiment to run
  ///---------------------------------------------
  std::cout<<"Run simulate at:" <<std::endl;
  std::cout<<"   1) 1.5T"       <<std::endl;
  std::cout<<"   2) 3.0T"       <<std::endl;
  std::cout<<"   3) 7.0T"       <<std::endl;
  std::cout<<"   4) 7.0T_PTX"   <<std::endl;
  std::cout<<"Select option: ";

  int i_option = 0;
  bool bGet= true;
  while(bGet){
    std::cin >> i_option;
    for(int i=1; i<5; i++){
      if (i_option==i){
        bGet=false;
      }
    }
  }


  ///---------------------------------------------
  /// Load dictionary
  ///---------------------------------------------
  std::vector<float> vf_dictionary;
  utility::load_bin("mrf_dictionary.float",   vf_dictionary );
  std::vector<float> vf_lookuptable;
  utility::load_bin("mrf_lookup_table.float", vf_lookuptable);

  long l_size_of_dictionary = vf_dictionary.size()/SIZE_OF_FINGERPRINT;


  ///---------------------------------------------
  /// Loop measurment data
  ///---------------------------------------------
  std::vector<float> vf_mask;    //Signal mask
  utility::load_bin("MODEL/mask.float", vf_mask);
  std::vector<float> vf_measdata;
  switch(i_option){
    case  1:
      utility::load_bin("CP_64Mhz_mrf_acquisition.float", vf_measdata);
     break;
    case  2:
      utility::load_bin("CP_128Mhz_mrf_acquisition.float", vf_measdata);
      break;
    case  3:
      utility::load_bin("CP_298Mhz_mrf_acquisition.float", vf_measdata);
      break;
    case  4:
      utility::load_bin("RF_298Mhz_mrf_acquisition.float", vf_measdata);
      break;
  }


  ///---------------------------------------------
  /// Allocate memory for reconstructed maps
  ///---------------------------------------------
  std::vector<float> vf_T1_map(vf_mask.size());  //Longitudinal relaxation time in  [ms]
  std::vector<float> vf_T2_map(vf_mask.size());  //Transverse relaxation time in    [ms]
  std::vector<float> vf_PD_map(vf_mask.size());  //Proton Density                   [a.u.]


  ///---------------------------------------------
  /// Match each voxel in the image
  ///---------------------------------------------
  for(int i=0; i<vf_mask.size(); i++){
    float cur_cor = -1.0f;
    float max_cor = -1.0f;
    int   match   = 0;

    ///---------------------------------------------
    /// Check if voxel is in mask
    ///---------------------------------------------
    if(vf_mask[i]>0.001){
      ///---------------------------------------------
      /// Find best match
      ///---------------------------------------------
      for(int j=0; j<l_size_of_dictionary; j++){
        dot_product(&vf_measdata[i*SIZE_OF_FINGERPRINT],&vf_dictionary[j*SIZE_OF_FINGERPRINT], &cur_cor);
        if(cur_cor > max_cor){
          max_cor    = cur_cor;
          match      = j;
        }
      }//j

      ///---------------------------------------------
      /// Extract properties
      ///---------------------------------------------
      dot_product(&vf_measdata[i*SIZE_OF_FINGERPRINT],&vf_measdata[i*SIZE_OF_FINGERPRINT],&vf_PD_map[i]);

      vf_T1_map[i] = vf_lookuptable[match*3+0];
      vf_T2_map[i] = vf_lookuptable[match*3+1];
      vf_PD_map[i] = sqrt(vf_PD_map[i])/vf_lookuptable[match*3+2];

    }else{ // voxel is outside mask
      vf_T1_map[i] = 0;
      vf_T2_map[i] = 0;
      vf_PD_map[i] = 0;
    }

    ///---------------------------------------------
    /// Show progress
    ///---------------------------------------------
    utility::progress_bar(i*1000.0f/(vf_mask.size()), 1000, 100, 10, "MRF reconstruction progress:");
  }//i


  ///---------------------------------------------
  /// Normalize PD map
  ///---------------------------------------------
  float   PDnorm  = 0;
  for(int i=0; i<vf_mask.size(); i++){
    if(vf_PD_map[i]>PDnorm){
      PDnorm = vf_PD_map[i];
    }
  }

  for(int i=0; i<vf_mask.size(); i++){
      vf_PD_map[i]/=PDnorm;
  }


  ///---------------------------------------------
  /// Save the results
  ///---------------------------------------------
  switch(i_option){
    case  1:
    utility::save_bin("Results/CP_64Mhz_MRF_T1.float"  ,vf_T1_map);
    utility::save_bin("Results/CP_64Mhz_MRF_T2.float"  ,vf_T2_map);
    utility::save_bin("Results/CP_64Mhz_MRF_PD.float"  ,vf_PD_map);
     break;
    case  2:
    utility::save_bin("Results/CP_128Mhz_MRF_T1.float"  ,vf_T1_map);
    utility::save_bin("Results/CP_128Mhz_MRF_T2.float"  ,vf_T2_map);
    utility::save_bin("Results/CP_128Mhz_MRF_PD.float"  ,vf_PD_map);
      break;
    case  3:
    utility::save_bin("Results/CP_298Mhz_MRF_T1.float"  ,vf_T1_map);
    utility::save_bin("Results/CP_298Mhz_MRF_T2.float"  ,vf_T2_map);
    utility::save_bin("Results/CP_298Mhz_MRF_PD.float"  ,vf_PD_map);
      break;
    case  4:
    utility::save_bin("Results/RF_298Mhz_MRF_T1.float"  ,vf_T1_map);
    utility::save_bin("Results/RF_298Mhz_MRF_T2.float"  ,vf_T2_map);
    utility::save_bin("Results/RF_298Mhz_MRF_PD.float"  ,vf_PD_map);
      break;
  }


  std::cout<<"MRF reconstruction progress:                Finished"<<std::endl;
  std::cout<<"===================================================="<<std::endl;

  return 0;
}


///---------------------------------------------
/// Dot product
///---------------------------------------------
void dot_product(float* left, float* right, float *res){
  *res = 0.0f;
  for(int i=0; i<SIZE_OF_FINGERPRINT; i++){
    *res += left[i]*right[i];
  }
}
