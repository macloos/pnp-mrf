%% Navigate to the folder containing the results

%% Load the mask
fileID = fopen('MODEL/mask.float','r');
mask = fread(fileID,160*160,'single');
mask = transpose(reshape(mask,160,160));
fclose(fileID);



%% Load the pnp mrf results
fileID = fopen('Results/RF_298Mhz_MRF_T1.float','r');
MRFT1 = fread(fileID,160*160,'single');
MRFT1 = mask.*transpose(reshape(MRFT1,160,160));
fclose(fileID);

fileID = fopen('Results/RF_298Mhz_MRF_T2.float','r');
MRFT2 = fread(fileID,160*160,'single');
MRFT2 = mask.*transpose(reshape(MRFT2,160,160));
fclose(fileID);

%% Load the pnp mrf results
fileID = fopen('Results/PNP_recon_T1.float','r');
PNPT1 = fread(fileID,160*160,'single');
PNPT1 = mask.*transpose(reshape(PNPT1,160,160));
fclose(fileID);

fileID = fopen('Results/PNP_recon_T2.float','r');
PNPT2 = fread(fileID,160*160,'single');
PNPT2 = mask.*transpose(reshape(PNPT2,160,160));
fclose(fileID);


figure;
subplot(2,2,1);
imshow(MRFT1,[0,3000]);
title('RF Shimmed MRF T1-map @ 7 Tesla');
colormap(jet);
colorbar;

subplot(2,2,2);
imshow(MRFT2,[0,200]);
title('RF Shimmed MRF T2-map @ 7 Tesla');
colormap(jet);
colorbar;

subplot(2,2,3);
imshow(PNPT1,[0,3000]);
title('PNP-MRF T1-map @ 7 Tesla');
colormap(jet);
colorbar;

subplot(2,2,4);
imshow(PNPT2,[0,200]);
title('PNP-MRF T2-map @ 7 Tesla');
colormap(jet);
colorbar;