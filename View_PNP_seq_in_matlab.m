%% Navigate to the SEQ_PNP folder before running script

%% Show the relative RF amplitudes
fileID = fopen('SEQ_PNP/TX1_amp.double','r');
TX1 = fread(fileID,480,'double');
fclose(fileID);

fileID = fopen('SEQ_PNP/TX2_amp.double','r');
TX2 = fread(fileID,480,'double');
fclose(fileID);

figure; 
plot([TX1,TX2]);
title('Realtive transmit amplitudes in the PnP-MRF sequence');
xlabel('Exposure Index');
ylabel('Normalized Amplitude');
legend('Channel 1','Channel2');

%% Show the RF phases
fileID = fopen('SEQ_PNP/TX1_pha.double','r');
Ph1 = fread(fileID,480,'double');
fclose(fileID);

fileID = fopen('SEQ_PNP/TX2_pha.double','r');
Ph2 = fread(fileID,480,'double');
fclose(fileID);

figure; 
plot([Ph1,Ph2]);
title('Transmit phase in the PnP-MRF sequence');
xlabel('Exposure Index');
ylabel('Phase');
legend('Channel 1','Channel2');

%% Show the TR
fileID = fopen('SEQ_PNP/TR.double','r');
TR = fread(fileID,480,'double');
fclose(fileID);

figure; 
plot(TR);
title('Inter echo time in the PnP-MRF sequence');
xlabel('Exposure Index');
ylabel('Time in ms');