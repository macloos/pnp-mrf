%% Navigate to the folder containing the results

%% Load the mask
fileID = fopen('MODEL/mask.float','r');
mask = fread(fileID,160*160,'single');
mask = transpose(reshape(mask,160,160));
fclose(fileID);

%% Load the gold standard
fileID = fopen('MODEL/T1_map.double','r');
T1g = fread(fileID,160*160,'double');
T1g = mask.*transpose(reshape(T1g,160,160));
fclose(fileID);

fileID = fopen('MODEL/T2_map.double','r');
T2g = fread(fileID,160*160,'double');
T2g = mask.*transpose(reshape(T2g,160,160));
fclose(fileID);

%% Load the pnp mrf results
fileID = fopen('Results/CP_298Mhz_MRF_T1.float','r');
T1 = fread(fileID,160*160,'single');
T1 = mask.*transpose(reshape(T1,160,160));
fclose(fileID);

fileID = fopen('Results/CP_298Mhz_MRF_T2.float','r');
T2 = fread(fileID,160*160,'single');
T2 = mask.*transpose(reshape(T2,160,160));
fclose(fileID);


figure;
subplot(2,2,1);
imshow(T1g,[0,3000]);
title('Goldstandard T1');
colormap(jet);
colorbar;

subplot(2,2,2);
imshow(T2g,[0,200]);
title('Goldstandard T2');
colormap(jet);
colorbar;

subplot(2,2,3);
imshow(T1,[0,3000]);
title('MRF T1-map @ 7 Tesla');
colormap(jet);
colorbar;

subplot(2,2,4);
imshow(T2,[0,200]);
title('MRF T2-map @ 7 Tesla');
colormap(jet);
colorbar;