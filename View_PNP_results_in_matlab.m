%% Navigate to the folder containing the results
close all;
clear all;

%% Load the mask
fileID = fopen('MODEL/mask.float','r');
mask = fread(fileID,160*160,'single');
mask = transpose(reshape(mask,160,160));
fclose(fileID);

%% Load the gold standard
fileID = fopen('MODEL/T1_map.double','r');
T1g = fread(fileID,160*160,'double');
T1g = mask.*transpose(reshape(T1g,160,160));
fclose(fileID);

fileID = fopen('MODEL/T2_map.double','r');
T2g = fread(fileID,160*160,'double');
T2g = mask.*transpose(reshape(T2g,160,160));
fclose(fileID);

fileID = fopen('MODEL/M1_298MHz.double','r');
L1g = fread(fileID,160*160,'double');
L1g = mask.*transpose(reshape(L1g,160,160));
fclose(fileID);

fileID = fopen('MODEL/M2_298MHz.double','r');
L2g = fread(fileID,160*160,'double');
L2g = mask.*transpose(reshape(L2g,160,160));
fclose(fileID);

figure;
subplot(2,2,1);
imshow(T1g,[0,3000]);
title('Goldstandard T1');
colormap(jet);
colorbar;

subplot(2,2,2);
imshow(T2g,[0,200]);
title('Goldstandard T2');
colormap(jet);
colorbar;

subplot(2,2,3);
imshow(L1g,[0,120]);
title('Goldstandard B1 for channel1');
colormap(jet);
colorbar;

subplot(2,2,4);
imshow(L2g,[0,120]);
title('Goldstandard B1 for channel2');
colormap(jet);
colorbar;


%% Load the pnp mrf results
fileID = fopen('Results/PNP_recon_T1.float','r');
T1 = fread(fileID,160*160,'single');
T1 = mask.*transpose(reshape(T1,160,160));
fclose(fileID);

fileID = fopen('Results/PNP_recon_T2.float','r');
T2 = fread(fileID,160*160,'single');
T2 = mask.*transpose(reshape(T2,160,160));
fclose(fileID);

fileID = fopen('Results/PNP_recon_M1.float','r');
L1 = fread(fileID,160*160,'single');
L1 = mask.*transpose(reshape(L1,160,160));
fclose(fileID);

fileID = fopen('Results/PNP_recon_M2.float','r');
L2 = fread(fileID,160*160,'single');
L2 = mask.*transpose(reshape(L2,160,160));
fclose(fileID);

figure;
subplot(2,2,1);
imshow(T1,[0,3000]);
title('PnP-MRF T1-map');
colormap(jet);
colorbar;

subplot(2,2,2);
imshow(T2,[0,200]);
title('PnP-MRF T2-map');
colormap(jet);
colorbar;

subplot(2,2,3);
imshow(L1,[0,120]);
title('PnP-MRF B1-map for channel1');
colormap(jet);
colorbar;

subplot(2,2,4);
imshow(L2,[0,120]);
title('PnP-MRF B1-map for channel2');
colormap(jet);
colorbar;